import { createContext } from 'react';
import { imagesData } from '../../db/data';

const ImagesDataContext = createContext([]);

const ImagesDataProvider = ({ children }) => {
  return (
    <ImagesDataContext.Provider value={imagesData}>
      {children}
    </ImagesDataContext.Provider>
  )
}

export  { ImagesDataContext, ImagesDataProvider };
