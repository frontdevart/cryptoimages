import './ImageItem.scss'

const ImageItem = ({ item }) => {
  return (
    <div className="image-item">
      <div className="card">
        <div className="imgBx">
          <img src={item.uri} alt={item.name}/>
        </div>
        <div className="contentBx">
          <h2>{item.name}</h2>
        </div>
      </div>
    </div>
  );
};

export default ImageItem;
