import { useContext } from 'react';
import ImageItem from '../ImageItem/ImageItem';
import { ImagesDataContext } from '../ImagesDataProvider/ImagesDataProvider';
import './Images.scss';

const Images = () => {
  const imagesData = useContext(ImagesDataContext);
  const cloneImagesData = imagesData.slice(0, 10);
  return (
    <div>
      {cloneImagesData.length > 0 && cloneImagesData.map((item, i) => (
        <ImageItem key={i} item={item} />
      ))}
    </div>
  );
};

export default Images;
