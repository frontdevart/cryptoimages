import Images from "./components/Images/Images";
import { ImagesDataProvider } from "./components/ImagesDataProvider/ImagesDataProvider";

const App = () => {
  return (
    <ImagesDataProvider>
      <div className="App">
        <Images />
      </div>
    </ImagesDataProvider>
  );
};

export default App;
